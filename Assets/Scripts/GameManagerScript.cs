﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerScript : MonoBehaviour
{
	public GameObject AllGameObjects;

	//int valueTime = 0;

	public Text TimeSeconds;
	public Text TimeMinutes;

	public GameObject GameCanvas;
	public GameObject MenuCanvas;
	public CubeManager CubeM;

	public int timersecond;
	private float secondgametimer;
	public float minedgametimer;

	public void PlayGame()
	{
		CubeM.Collect();
		secondgametimer = 0;
		timersecond = 0;
		GameCanvas.SetActive(true);
		AllGameObjects.SetActive(true);
		MenuCanvas.SetActive(false);
	}
	public void ExitGame()
	{
		Debug.Log("Выйти");
		Application.Quit();
	}

	public void ExitMainMenu()
	{
		CubeM.Collect();
		MenuCanvas.SetActive(true);
		GameCanvas.SetActive(false);
		AllGameObjects.SetActive(false);

	}

	private void Update()
	{
		secondgametimer += Time.deltaTime;

		if(secondgametimer >= 1)
		{
			timersecond += 1;
			secondgametimer = 0;
		}
		if(timersecond >= 60)
		{
			minedgametimer += 1;
			timersecond = 0;
		}
		

		TimeSeconds.text = "Seconds: " +  (timersecond).ToString();
		TimeMinutes.text = "Minutes: " +  (minedgametimer).ToString();
	}

}
